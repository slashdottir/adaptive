package com.adaptive;

import com.adaptive.WordCounter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {

    public static boolean test1(WordCounter wc, String url, List<String> keywords, int expectedCount) {
        int count = wc.getKeywordCount(keywords, url);
        int count2 = wc.getKeywordCount(keywords, url);
        return (count == count2) && (count2 == expectedCount);
    }

    public static class TestThread implements Runnable{
        private WordCounter _wc = null;
        private String url = null;
        private List<String> keywords = null;
        private int expectedCount = 0;
        public boolean status = true;
        public TestThread(WordCounter wc, String url, List<String> keywords, int expectedCount) {
            this._wc = wc;
            this.url = url;
            this.keywords = keywords;
            this.expectedCount = expectedCount;
        }
        public void run() {
            if ( ! test1(this._wc, this.url, this.keywords, this.expectedCount) ) {
                this.status = false;
            }
        }
        public boolean success() {
            return this.status;
        }
    }

    public static boolean test2(WordCounter wc) {
        List<Thread> threads = new ArrayList<Thread>();
        List<TestThread> tests = new ArrayList<TestThread>();
        List<String> urls = Arrays.asList(
                "https://en.wikipedia.org/wiki/Abyssomyces",
                "https://en.wikipedia.org/wiki/Hex_Castle",
                "https://en.wikipedia.org/wiki/Alps%E2%80%93Mediterranean_Euroregion"
        );
        List<List<String>> keys = Arrays.asList(
                Arrays.asList("unknown"),
                Arrays.asList("house"),
                Arrays.asList("Italian")
        );
        List<Integer> counts = Arrays.asList(1,2,6);

        try {
            for (int i=0; i < 3; i++) {
                TestThread tt = new TestThread(wc, urls.get(i), keys.get(i), counts.get(i));
                tests.add(tt);
                Thread t = new Thread(tt);
                threads.add(t);
                t.start();
            }

            for(int i = 0; i < threads.size(); i++) {
                Thread t = threads.get(i);
                t.join();
            }
        }
        catch (Exception ex) {
            return false;
        }
        for(int i = 0; i < tests.size(); i++) {
            TestThread t = tests.get(i);
            if ( ! t.success() ) {
                return false;
            }
        }

        return true;
    }

    public static void main(String[] args) {
        WordCounter wc = new WordCounter();
        if ( ! test1(wc, "https://en.wikipedia.org/wiki/Ithirameru",
                Arrays.asList("Province"), 7) ) {
            System.exit(1);
        }
        if ( ! test2(wc) ) {
            System.exit(2);
        }
    }
}
