package com.adaptive;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.lang.ref.SoftReference;
import java.time.Duration;
import java.time.Instant;
import java.util.*;
import java.net.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/*
** WORDCOUNTER.JAVA
**
** Library object that, given a url and set of keywords, returns the number of times
** any of these keywords appear in the resource identified by the URL.
** Only full-word matches should "hit".
** Assume that words are delimited by regex word-boundary characters (i.e. '\b')
**
** The library must support multi-threaded access.
** Once URL resources are loaded, the URL data should be cached for subsequent requests.
** A request should refresh any cached URL data if the data is at least an hour old.
**
** Should stand alone with no additional dependencies beyond what is in the JRE.
*/

public class WordCounter {
    private Logger log = Logger.getLogger(String.valueOf(this.getClass()));

    private final ConcurrentHashMap<String, SoftReference<urlData>> cache
            = new ConcurrentHashMap<String, SoftReference<urlData>>();

    class urlData {
        public Instant time;
        public int keywords_count;

        public urlData(int kwcount) {
            this.time = Instant.now();
            this.keywords_count = kwcount;
        }

        public boolean isExpired() {
            Instant now = Instant.now();
            Duration timeElapsed = Duration.between(this.time, now);
            return timeElapsed.toMinutes() > 60;
        }
    }

    public synchronized int getKeywordCount(List<String> keywords, String url) {
        final String lookup = url + "|" + makeHashOfKeywords(keywords);
        if (cache.containsKey(lookup)) {
            urlData data = cache.get(lookup).get();
            if (!data.isExpired()) {
                return data.keywords_count;
            }
            cache.remove(lookup);
        }
        final Set<String> set = new HashSet<String>(keywords);
        String urlContents = "";
        try {
            urlContents = getTextFromUrl(url);
        } catch (Exception ex) {
            final String msg = this.getClass().getName() + ":" + ex.getMessage();
            log.warning(msg);
            return -1;
        }
        urlContents = stripHTML(urlContents);
        final int count = getKeywordMatchCount(set, urlContents);
        urlData data = new urlData(count);
        SoftReference<urlData> sr = new SoftReference<urlData>(data);
        cache.put(lookup, sr);
        return count;
    }

    public int getKeywordMatchCount(final Set<String> keywords, final String text) {
        final String pattern = "\\b(<word>)\\b";
        int count = 0;
        for (final String keyword : keywords) {
            final String search = pattern.replaceAll("<word>", keyword);
            final Matcher m = Pattern.compile(search).matcher(text);
            int matchCount = 0;
            while (m.find()) {
                matchCount += 1;
            }
            count += matchCount;
        }
        return count;
    }

    public String stripHTML(final String text) {
        final String pattern = "<[^>]*>";
        final String text2 = text.replaceAll(pattern, "");
        return text2;
    }

    public String makeHashOfKeywords(List<String> keywords) {
        int hash = 0;
        Collections.sort(keywords);
        for (String keyword : keywords) {
            hash |= keyword.hashCode();
        }
        String result = "" + hash;
        return result;
    }

    public static String getTextFromUrl(final String url) throws Exception {
        URL website = new URL(url);
        URLConnection connection = website.openConnection();
        BufferedReader in = new BufferedReader(
                new InputStreamReader(
                        connection.getInputStream()));

        StringBuilder response = new StringBuilder();
        String inputLine;

        while ((inputLine = in.readLine()) != null)
            response.append(inputLine);

        in.close();
        return response.toString();
    }
}
